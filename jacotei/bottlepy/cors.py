# -*- coding: utf-8 -*-
from bottle import request, response


class CorsPlugin(object):
    """
    CORS plugin for Bottle.py
    """
    api = 2
    name = 'cors'

    def __init__(self, allowed_domains):
        self.app = None
        self.allowed_domains = allowed_domains

    def setup(self, app):
        self.app = app

    def apply(self, callback, context):
        if not hasattr(callback, 'cors_enable') or not callback.cors_enable:
            return callback
        else:
            if not hasattr(callback, 'cors_methods'):
                callback.cors_methods = []
            callback.cors_methods.append(context.method)

        def wrapper(*args, **kwargs):
            origin = request.headers.get('Origin')
            if origin and any(domain in origin for domain in self.allowed_domains):
                response.headers['Access-Control-Allow-Origin'] = origin
                if hasattr(callback, 'cors_methods') and callback.cors_methods:
                    response.headers['Access-Control-Allow-Methods'] = ', '.join(callback.cors_methods)
                if hasattr(callback, 'cors_allow_credentials') and callback.cors_allow_credentials:
                    response.headers['Access-Control-Allow-Credentials'] = 'true'
                else:
                    response.headers['Access-Control-Allow-Credentials'] = 'false'
            # else:
            #     abort(401, 'Forbidden! Unrecognized Origin.')

            if request.method == 'OPTIONS':
                return None
            return callback(*args, **kwargs)
        return wrapper

    def enable(self, callback):
        """
        Enable CorsPlugin for selected route
        :param callback:
        :return: callback
        """
        callback.cors_enable = True
        return callback
        
    def allow_credentials(self, callback):
        """
        Allow CorsPlugin to receive credential cookies (eg: Login)
        :param callback:
        :return: callback
        """
        callback.cors_allow_credentials = True
        return callback
