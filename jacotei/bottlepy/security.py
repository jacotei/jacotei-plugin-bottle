# -*- coding: utf-8 -*-
import requests
from bottle import abort, request

COOKIE_SESSION_ID = 'sid'
PARAM_API_KEY = 'x-api-key'
PARAM_SESSION_ID = 'x-sid'


class SecurityPlugin(object):
    api = 2
    name = 'security'

    def __init__(self, x_api_key, cache=None):
        """
        :type x_api_key: API Key que a aplicacao vai usar para se conectar a api de auth
        :type cache: Instancia de cache que respente api do memcached
        """
        self.app = None
        self.x_api_key = x_api_key
        self.cache = cache
        self.headers = dict()
        self.headers[PARAM_API_KEY] = self.x_api_key

    def setup(self, app):
        self.app = app

    def apply(self, callback, context):
        if hasattr(callback, 'ignore_security') and callback.ignore_security is True:
            return callback

        def wrapper(*args, **kwargs):
            if self.__authenticate():
                return callback(*args, **kwargs)
            else:
                abort(401, 'Forbidden! Please provide a valid x-api-key.')

        return wrapper

    def __authenticate(self):
        r = None
        api_key = request.params.get(PARAM_API_KEY, request.headers.get(PARAM_API_KEY))
        if api_key:
            if PARAM_API_KEY in request.params:
                del request.params[PARAM_API_KEY]
            r = requests.get('http://auth.api.jacotei.com.br/api-key/%s' % api_key, headers=self.headers)
        else:
            session_id = request.cookies.get(COOKIE_SESSION_ID, request.params.get(PARAM_SESSION_ID, request.headers.get(PARAM_SESSION_ID)))
            if session_id:
                r = requests.get('http://auth.api.jacotei.com.br/session/%s' % session_id, headers=self.headers)

        if r:
            if r.status_code == 200:
                return True
            elif r.status_code != 404:
                r.raise_for_status()

        return False

    def disable(self, callback):
        """
        Disable security for selected route
        :param callback:
        :return: callback
        """
        callback.ignore_security = True
        return callback
