#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Jun 8, 2015

@author: marcos
"""
from os import path
from setuptools import setup, find_packages
from pip.req import parse_requirements
import subprocess

BASE_PATH = path.dirname(path.realpath(__file__))

REQUIREMENTS = parse_requirements(path.join(BASE_PATH, 'requirements.txt'), session=False)
INSTALL_REQUIRES = [str(ir.req) for ir in REQUIREMENTS]


def find_version():
    version = None
    try:
        git_version = subprocess.check_output('git describe --tags'.split()).strip('\n')[1:].rsplit('-', 2)
        version = git_version[0]
        if git_version[1]:
            version += '.dev%s' % git_version[1]
    except:
        pass

    if not version:
        if path.isfile('version.txt'):
            with open('version.txt') as f:
                version = f.read()
        else:
            version = 'SNAPSHOT'

    with open('version.txt', 'w') as f:
        f.write(version)

    return version


setup(
    name="jacotei-plugin-bottle",
    version=find_version(),

    author="JáCotei",
    author_email="suporte@jacotei.com.br",
    description="JaCotei Bottle Plugins",

    url="https://bitbucket.org/jacotei/jacotei-plugin-bottle",
    license="Proprietary",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Environment :: Web Environment",
        "Topic :: Internet :: WWW/HTTP",
        "License :: Other/Proprietary License",
    ],

    packages=find_packages(),
    package_data={'': ['requirements.txt']},
    include_package_data=True,
    install_requires=INSTALL_REQUIRES,

    zip_safe=False
)
